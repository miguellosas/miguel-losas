<?php

use cursophp7\app\utils\MyLog;
use cursophp7\core\App;
use cursophp7\core\Router;

session_start();

require __DIR__ . '/../vendor/autoload.php';


$config = require_once __DIR__ . '/../app/config.php';
App::bind('config',$config);

$router = Router::load(__DIR__ . '/../app/' . $config['routes']['filename']);
App::bind('router',$router);

$logger = MyLog::load(__DIR__ . '/../logs/' . $config['logs']['filename'], $config['logs']['level']);
App::bind('logger',$logger);

$mailer = new MyMail();
App::bind('mailer', $mailer);

if(isset($_SESSION['loguedUser']))
    $appUser = App::getRepository(\cursophp7\app\repository\UsuarioRepository::class)->find($_SESSION['loguedUser']);
else
    $appUser = null;


    App::bind('appUser', $appUser);