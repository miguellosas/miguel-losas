<?php
/*require_once __DIR__ . '/../exceptions/QueryException.php';
require_once __DIR__ . '/../core/App.php';*/

namespace cursophp7\core\database;


 use cursophp7\app\exceptions\NotFoundException;
 use cursophp7\app\exceptions\QueryException;
 use cursophp7\core\App;
 use PDO;
 use PDOException;

 abstract class QueryBuilder
{
    private $connection;

    /**
     * @var
     */
    private $table;
    /**
     * @var
     */
    private $classEntity;


    public function __construct(string $table, string $classEntity)
    {

        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

     /**
      * @param string $sql
      * @return array
      * @throws QueryException
      */
    private function executeQuery(string $sql, array $parameters = []) : array
    {
        $pdoStatement = $this->connection->prepare($sql);

        if($pdoStatement->execute($parameters) === false)
            throw new QueryException("No se ha podido solicitar la query solicitada");

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);

    }

    /**
     * @param string $table
     * @param string $classEntyty
     * @return array
     * @throws QueryException
     */

    public function findAll(): array
    {
        $sql = "SELECT * from $this->table";

        return $this->executeQuery($sql);

    }

     /**
      * @param int $id
      * @return IEntity
      * @throws NotFoundException
      * @throws QueryException
      */
    public function find(int $id): IEntity
    {
       $sql = "SELECT * from $this->table WHERE id=$id";

        $result = $this->executeQuery($sql);

        if(empty($result))

            throw  new  NotFoundException("No se ha encontrado ningún elemento con id $id" );

        return $result[0];
    }


    private function getFilters(array $filters)
    {
        if(empty($filters))
            return '';

        $strFilters = [];

        foreach ($filters as $key=>$value)
            $strFilters[] = $key . '=:' .$key;

        return 'WHERE' . implode(' and ', $strFilters);



    }


       public function findBy(array $filters):array
       {
           $sql = "SELECT * from $this->table "  . $this->getFilters($filters);

          return   $this->executeQuery($sql, $filters);


       }

        public function findOneBy(array $filters):?IEntity
        {
            $result = $this->findBy($filters);

            if(count($result) > 0)
                return $result[0];

            return null;

        }




    public function save(IEntity $entity): void
    {
            try {


            $parameters = $entity->toArray();
            $sql = sprintf(
                'insert into %s (%s) values (%s)',
                $this->table,
                implode(',', array_keys($parameters)),
                ':' . implode(', :', array_keys($parameters))


            );
            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);
        }

    catch (PDOException $exception)
        {
            throw new QueryException('Error al insertar en la base de datos');

        }


    }

     /**
      * @param array $parameters
      * @return string
      */
    private function getUpdates(array $parameters)
    {
        $updates ='';

        foreach ($parameters as $key => $value)
        {
                if($key !== 'id')
                {
                    if($updates !=='')
                        $updates =', ';
                      $updates .= $key . '=:' . $key;

                }

        }

        return $updates;

    }


     /**
      * @param IEntity $entity
      * @throws QueryException
      */
    public function update(IEntity $entity) :void
    {
        try{

            $parameters = $entity->toArray();

            $sql = sprintf(
                'insert intos %s SET %s WHERE id=id',
                $this->table,
                $this->getUpdates($parameters)
            );

            $statement =  $this->connection->prepare($sql);

            $statement->execute($parameters);

        }
        catch(PDOException $pdoException)
        {
            throw new QueryException('Error al actualizar el elemento con id ' . $parameters['id']);
        }

    }


    public function executeTransaction(callable $fnExecuteQuerys)
    {

        try
        {
            $this->connection->beginTransaction();

            $fnExecuteQuerys();

            $this->connection->commit();
        }
        catch (PDOException  $PDOException  )
        {
                $this->connection->rollBack();

                throw new QueryException('No se ha podido realizar la operacion');
        }

    }








}