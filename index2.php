<?php
require 'utils/utils.php';

require 'views/index.view.php';



$imagenes = [

    new ImagenGaleria('1.jpg',
        'Descripción imagen 1',
        50 ,
        0,
        35),


    new ImagenGaleria(
        '2.jpg',
        'Descripción imagen 2',
        41,
        5,
        3),

    new ImagenGaleria(
        '3.jpg',
        'Descripción imagen 3',
        40,
        2,
        5),


    new ImagenGaleria(
        '4.jpg',
        'Descripción imagen 4',
        33,
        23,
        44),

    new ImagenGaleria(
        '5.jpg',
        'Descripción imagen 5',
        50,
        0,
        35),


    new ImagenGaleria(
        '6.jpg',
        'Descripción imagen 6',
        50,
        0,
        35),


    new ImagenGaleria(
        '7.jpg',
        'Descripción imagen 7',
        50,
        0,
        35),



    new ImagenGaleria(
        '8.jpg',
        'Descripción imagen 8',
        50 ,
        0 ,
        35),


    new ImagenGaleria(
        '9.jpg',
        'Descripción imagen 9',
        50,
        0 ,
        35),


    new ImagenGaleria(
        '10.jpg',
        'Descripción imagen 10',
        50,
        0 ,
        35 ),


    new ImagenGaleria(
        '11.jpg',
        'Descripción imagen 11',
        50 ,
        0,
        35),


    new ImagenGaleria(
        '12.jpg',
        'Descripción imagen 12',
        50 ,
        0 ,
        35),

] ;

