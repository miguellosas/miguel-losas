<?php

namespace cursophp7\app\controllers;

use cursophp7\app\entity\Asociado;
use cursophp7\app\entity\Mensaje;
use cursophp7\app\exceptions\FileException;
use cursophp7\app\exceptions\QueryException;
use cursophp7\app\exceptions\ValidationException;
use cursophp7\app\repository\AsociadoRepository;
use cursophp7\app\repository\MensajeRepository;
use cursophp7\app\utils\File;
use cursophp7\core\App;
use cursophp7\core\Response;

class MensajeController
{
    /**
     * @throws QueryException
     */

    public function index()
    {
        findAll();

        Response::renderView('acontact', 'layout',

        );
    }

    /**
     * @throws QueryException
     * @throws ValidationException
     * @throws FileException
     */
    public function nuevo()
    {
        try {

            $nombre = trim(htmlspecialchars($_POST['nombre']));
            $apellidos = trim(htmlspecialchars($_POST['apellidos']));
            $email = trim(htmlspecialchars($_POST['email']));
            $asunto = trim(htmlspecialchars($_POST['asunto']));
            $texto = trim(htmlspecialchars($_POST['texto']));

            if (empty($nombre))
                $errores[] = "El nombre no se puede quedar vacío";

            if (empty($email))
                $errores[] = "El e-mail no se puede quedar vacío";


            else {
                if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)

                    throw new ValidationException('El e-mail no  valido ');


            }


            if (empty($asunto))
                $errores[] = "El asunto no se puede quedar vacío";

            if (empty($errores)) {
                $mensaje = new Mensaje($nombre, $apellidos, $asunto, $email, $texto);

                App::getRepository((MensajeRepository::class))->save($mensaje);

                $message = "Se ha guardado una nuevo mensaje: " . $mensaje->getTexto();
                App::get('logger')->add($message);


            }

        } catch (ValidationException $validationException) {
            die($validationException->getMessage());

        }
        App::get('router')->redirect('contact');

    }

















}