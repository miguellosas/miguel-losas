<?php

namespace cursophp7\app\controllers;



use cursophp7\app\entity\Usuario;
use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\QueryException;
use cursophp7\app\exceptions\ValidationException;
use cursophp7\app\repository\UsuarioRepository;
use cursophp7\core\App;
use cursophp7\core\helpers\FlashMessage;
use cursophp7\core\Response;
use cursophp7\core\Security;

class AuthController
{

    public function login()
    {
        $errores = FlashMessage::get('login-error', []);
        $username = FlashMessage::get('username');

        Response::renderView('login','layout',compact('errores','username'));

    }

    /**
     * @throws AppException
     */

    public function checkLogin()
    {
        try {
            if (!isset($_POST['username']) || empty($_POST['username']))
                throw new ValidationException('Debes introducir el usuario y el password');

            FlashMessage::set('username', $_POST['username']);


            if( !isset($_POST['password']) || empty($_POST['username']))
        throw new ValidationException('Debes introducir el usuario y el password');




        $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([

            'username' => $_POST['username']

            ]



        );


        if (!is_null($usuario) && Security::checkPassword($_POST['password'],$usuario->getPassword())) {

            $_SESSION['loguedUser'] = $usuario->getId();

            FlashMessage::unset('username');


            App::get('router')->redirect('');


        }



        throw new ValidationException('eL USUARIO Y EL PASSWORD INTRODUCIDOS NO EXISTEN');

        }
        catch(ValidationException $validationException)
        {
            FlashMessage::set('login-error', [$validationException->getMessage()]);

            App::get('router')->redirect('login');
        }

    }

    /**
     * @throws AppException
     */

    public function logout()
    {
        if(isset($_SESSION['loguedUser']))
        {
            $_SESSION['loguedUser'] = null;
            unset($_SESSION['loguedUser']);

        }

        App::get('router')->redirect('login');

    }


    /**
     * @throws AppException
     */
    public function registro()
    {
        $errores =FlashMessage::get('registro-error', []);
        $username = FlashMessage::get('username');


        Response::renderView('registro','layout', compact('errores','username'));

    }

    /**
     * @throws AppException
     * @throws QueryException
     */

    public function checkRegistro()
    {
        try{


            if (!isset($_POST['username'])
                || empty($_POST['username']))
            throw new ValidationException('El nombre de usuario no puede quedar vacío');

            FlashMessage::set('username',$_POST['username']);


            if (!isset($_POST['password'])
                || empty($_POST['password']))
                throw new ValidationException('El password no puede quedar vacío ');


            if (!isset($_POST['re-password'])
                || empty([$_POST['re-password']])
                || $_POST['password'] !== $_POST['re-password'])
                throw new ValidationException('Ambos password deben coincidir');

            $password = Security::encrypt ($_POST['password']);

            $usuario = new Usuario();
            $usuario->setUsername($_POST['username']);
            $usuario->setRole('ROLE_USER');
            $usuario->setPassword($password);

            App::getRepository(UsuarioRepository::class)->save($usuario);

            FlashMessage::unset('username');

            App::get('router')->redirect('login');



        }

        catch (ValidationException $validationException)
        {
            FlashMessage::set('registro-error' , [$validationException->getMessage() ]);

               App::get('router')->redirect('registro');
        }



    }



}