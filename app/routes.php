<?php
$router->get('','PagesController@index');
$router->get('about' , 'PagesController@about');
$router->get('asociados' ,'AsociadoController@index', 'ROLE_USER');
$router->post('asociados/nuevo' ,'AsociadoController@nuevo','ROLE_ADMIN');
$router->get('blog' , 'PagesController@blog');
$router->get('contact' ,'MensajeController@index');
$router->post('contact/nuevo' ,'MensajeController@nuevo');
$router->get('imagenes-galeria' , 'ImagenGaleriaController@index','ROLE_USER');
$router->get('imagenes-galeria/:id' , 'ImagenGaleriaController@show','ROLE_USER');

$router->get('post' , 'PagesController@post');
$router->post('imagenes-galeria/nueva', 'ImagenGaleriaController@nueva','ROLE_ADMIN');
$router->get('login' , 'AuthController@login');
$router->post('check-login' , 'AuthController@checklogin');
$router->get('logout','AuthController@logout','ROLE_USER');
$router->get('registro', 'AuthController@registro');
$router->post('check-registro', 'AuthController@checkRegistro');