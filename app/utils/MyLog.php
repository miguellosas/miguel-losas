<?php
namespace cursophp7\app\utils;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MyLog
{
    /**
     * @var Logger
     */
    private $log;

    private $level;

    /**
     * MyLog constructor.
     * @param string $filename
     * @throws Exception
     */




    private function  __construct(string $filename, int $level)
    {
        $this->level = $level;
        $this->log = new Logger('name');
        $this->log->pushHandler(

            new StreamHandler($filename, $this->level)

        );

    }

    /**
     * @param string $filename
     * @return MyLog
     * @throws Exception
     */

    public static function load(string $filename, int $level = Logger::INFO) :MyLog
    {

        return new MyLog($filename,$level );

    }

    /**
     * @param string $message
     */

    public  function add(string $message):void
    {
        $this->log->log( $this->level,$message);


    }



}