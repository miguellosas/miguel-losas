<?php

use Monolog\Logger;

return [
  'database' => [
      'name' => 'cursophp7',
      'username' =>'userCurso',
      'password' => 'php',
      'connection' => 'mysql:host=miguel-losas.local',
      'options' => [

          PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_PERSISTENT  => true,
      ],


  ],

    'logs' => [
        'filename' => 'curso.log',
        'level' => Logger::INFO
        ],
    'routes' => [
        'filename' => 'routes.php'

    ],

    'project' => [

        'namespace' => 'cursophp7'

    ],

    'swiftmail' => [

        'smtp_server' => 'smtp.gmail.com',
    'smtp_port' => '587',
    'smtp_security' => 'tls',
    'username' => 'cursophp7cefire@gmail.com',
    'password' => 'alex3333',
    'email' => 'cursophp7cefire@gmail.com',
    'name' => 'Curso PHP 7'
 ],

    'security' => [
        'roles' => [
            'ROLE_ADMIN' => 3,
            'ROLE_USER' => 2,
            'ROLE_ANONYMOUS' => 1,

        ]

    ]



];
