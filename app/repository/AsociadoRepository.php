<?php
namespace cursophp7\app\repository;

use cursophp7\app\entity\Asociado;
use cursophp7\core\database\QueryBuilder;

class AsociadoRepository extends QueryBuilder
{

    public function __construct(string $table='asociados', string $classEntity=Asociado::class)
    {
        parent::__construct($table, $classEntity);
    }

}