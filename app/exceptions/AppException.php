<?php
namespace cursophp7\app\exceptions;

use Exception;
use Throwable;

class AppException extends Exception
{

    public function __construct($message = "", $code = 500)
    {
        parent::__construct($message, $code);

    }


    private function getHttpHeaderMessage()
    {

        switch($this->getCode())
        {

            case 404:
                return '404 Not Found';

            case 403:
                return '403 Forbidden';

            case 500:
                return '500 Internal Server Error';

        }


    }

    public function handleError()
    {
        try{

            $httHeaderMessage = $this->getHttpHeaderMessage();
            header($_SERVER['SERVER_PROTOCOL'] . ' ' . $httpHeaderMessage, true, $this->getCode() );

            $errorMessage = $this->getMessage();

        }



    }




}